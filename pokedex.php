<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="css/estilo.css" type="text/css">
	<title>Pokedex</title>
</head>
<?PHP
	$pokemones = array(	"charmander" =>["Foto" =>'<img src = "img/Charmander.png"/>', "Tipo" => "fuego","Genero" => "M","Ataque" => "Llamarada"],
						"pikachu" =>["Foto" =>'<img src = "img/Pikachu.png"/>', "Tipo" => "Electrico", "Genero" => "M", "Ataque" => "ElectroVolt"],
						"bulbasaur" =>["Foto" =>'<img src = "img/Bulbasaur.png"/>', "Tipo" => "Hoja","Genero" => "F","Ataque" => "Latigo Sepa"]);

					if (!empty ($_GET ["nombre"])){	
						$buscado = $_GET ["nombre"];
					};
					
				?>

<body>
	<img src="img/fondo.png">
	<div class="buscador">
		<form action="pokedex.php" method="get">
			BUSCAR : <input type="text" name="nombre">
			<input class="w3-button w3-red w3-round-large" type="submit" value="ENVIAR">
		</form>
		<table class="w3-table">

			<tr>
				<th>Pokemon</th>
				<th>Tipo</th>
				<th>Genero</th>
				<th>Ataque</th>
			</tr>
			<tr>
				<td>
					<div class= "foto">
					<?php
						error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_WARNING & ~E_NOTICE);
						echo $pokemones[$buscado]["Foto"]; 
					?>
					</div>
					
				</td>
				<td>
					<?php
						echo $pokemones[$buscado]["Tipo"];
					?>
				</td>

				<td>
					<?php
						echo $pokemones[$buscado]["Genero"];
					?>
				</td>
				<td>
					<?php
						echo $pokemones[$buscado]["Ataque"];
					?>
				</td>
			</tr>
		</table>
	</div>
</body>

</html>
